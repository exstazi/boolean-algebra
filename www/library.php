<?php
/*
	- letter( $x ) 
	- sign( $x )
	- arr_search( $arr, $x )
	- prints( $x )
	- priority( $x );
*/
function print_a( $arr )
{
	echo '<pre>';
	print_r( $arr );
	echo '</pre>';
}

function letter( $x )
{
	if( 'A' <= $x && $x <= 'Z' )		return true;
	return false;
}

function sign( $x )
{
	if( $x == '!' or $x == '*' or $x == '/' or $x == '+' or  $x == '%' or $x == '?' or $x == '^' or $x == '|' )		return true;
	return false;
}

function arr_search( $arr, $x )
{
	foreach ( $arr as &$value ) 
	{
		if( $value == $x )			return true;
	}
	return false;
}

function prints( $x )
{
	$j = strlen( $x );
	$y = '';
	for( $i = 0; $i < $j; $i++ )
	{
			if( $x[$i] == '+' )	$y.= '&or;';
		elseif( $x[$i] == '*' )	$y.= '&and;';
		elseif( $x[$i] == '/' ) $y.= '&oplus;';
		elseif( $x[$i] == '?' ) $y.= '&rarr;';
		elseif( $x[$i] == '%' ) $y.= '&harr;';
		elseif( $x[$i] == '#' ) $y.= '&darr;';
		elseif( $x[$i] == '!' ) $y.= '&not;';
		else					$y.= $x[$i];
	}
	return $y;
}


function priority( $x )
{
		if( $x == '!' )		return 7;
	elseif( $x == '*' )		return 6;
	elseif( $x == '+' )		return 5;
	elseif( $x == '/' ) 	return 4;
	elseif( $x == '?' ) 	return 3;
	elseif( $x == '%' )		return 2;
	elseif( $x == '^' )		return 1;
	elseif( $x == '|' )		return 0;
	return -1;
}


function invention( $x )
{
	$i = strlen( $x );
	$str = '';
	for( ; $i >= 0; $i-- )
	{
		if( $x[$i] == '(' )
			$str.= ')';
		elseif( $x[$i] == ')' )
			$str.= '(';
		else
			$str.= $x[$i];
	}
	return $str;
}
?>