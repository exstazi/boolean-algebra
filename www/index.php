<!DOCTYPE html>
<HTML>
<HEAD>
<script>

function tabSwitch(new_tab, new_content) {
    document.getElementById('content_1').style.display = 'none';
    document.getElementById('content_2').style.display = 'none';
    document.getElementById('content_3').style.display = 'none';
    document.getElementById(new_content).style.display = 'block';   
    
    document.getElementById('tab_1').className = '';
    document.getElementById('tab_2').className = '';
    document.getElementById('tab_3').className = '';
    document.getElementById(new_tab).className = 'active';
}
</script>

<style>
body {
	background-image:url(site/background.jpg);
	background-repeat:no-repeat;
	background-position:top center;
	background-color:#657077;
	margin:40px;
}
.tabbed_box {
        margin: 0px auto 0px auto;
        width:800px;
}
.tabbed_box h4 {
        font-family:Arial, Helvetica, sans-serif;
        font-size:23px;
        color:#ffffff;
        letter-spacing:-1px;
        margin-bottom:10px;
}
.tabbed_box h4 small {
        color:#e3e9ec;
        font-weight:normal;
        font-size:9px;
        font-family:Verdana, Arial, Helvetica, sans-serif;
        text-transform:uppercase;
        position:relative;
        top:-4px;
        left:6px;
        letter-spacing:0px;
}
.tabbed_area {
        border:1px solid #494e52;
        background-color:#636d76;
        padding:8px;
}
ul.tabs {
        margin:0px; padding:0px;
        margin-top:5px;
        margin-bottom:5px;
}
ul.tabs li {
        list-style:none;
        display:inline;
}
ul.tabs li a {
        background-color:#464c54;
        color:#ffebb5;
        padding:8px 14px 8px 14px;
        text-decoration:none;
        font-size:9px;
        font-family:Verdana, Arial, Helvetica, sans-serif;
        font-weight:bold;
        text-transform:uppercase;
        border:1px solid #464c54;
}
ul.tabs li a:hover {
        background-color:#2f343a;
        border-color:#2f343a;
}
ul.tabs li a.active {
        background-color:#ffffff;
        color:#282e32;
        border:1px solid #464c54;
        border-bottom: 1px solid #ffffff;
}
.content {
        background-color:#ffffff;
        padding:10px;
        border:1px solid #464c54;
}
#content_2, #content_3 { display:none; }
.inputBox {
	padding: 6px;
	width: 95%;
	padding: 6px;
	font-size:16px;
	text-decoration: none;
	text-transform: uppercase;
	font-family: 'Oswald', sans-serif;
	font-weight: 300;
}
</style>
</HEAD>
<body>
<div id="tabbed_box_1" class="tabbed_box">
    <h4>������ ����������� <small>�� �������������� ������</small></h4>
    <div class="tabbed_area">

        <ul class="tabs">
            <li><a href="javascript:tabSwitch('tab_1', 'content_1');" id="tab_1" class="active">�������</a></li>
            <li><a href="javascript:tabSwitch('tab_2', 'content_2');" id="tab_2">�����������</a></li>
            <li><a href="javascript:tabSwitch('tab_3', 'content_3');" id="tab_3">������</a></li>
        </ul>

        <div id="content_1" class="content">
			<form action="result.php" method="GET" target="_blank">
			<input type="text" name="formula" class="inputBox" />
			<input type="submit" value="���������">
			</form>
		</div>
        <div id="content_2" class="content">
		<pre>
	��� ������� ����� ������� ��������� �����������:
		<table border=0>
		<tr>
		<td>&not; (���������) </td>
		<td width=200>- (!)</td>
		<td>&not;x   - !x</td>
		</tr>
		<tr>
		<td>&or; (���������� ���) </td>
		<td>- (+) ��� (or)</td>
		<td>x&or;y  -  x+y</td>
		</tr>
		<tr>
		<td>&and; (���������� �) </td>
		<td>- (*) ��� (and)</td>
		<td>x&and;y  -  x*y</td>
		</tr>
		<tr>
		<td>&oplus; (�������� �� ������) </td>
		<td>- (/) ��� (xor)</td>
		<td>x&oplus;y  -  x/y</td>
		</tr>
		<tr>
		<td>&rarr; (���������) </td>
		<td>- (?) ��� (->)</td>
		<td>x&rarr;y  -  x->y</td>
		</tr>
		<tr>
		<td>&harr; (��������������) </td>
		<td>- (%) ��� (<->)</td>
		<td>x&harr;y  -  x<->y</td>
		</tr>
		<tr>
		<td>&darr; (������� �����) </td>
		<td>- (^)</td>
		<td>x&darr;y  -  x^y</td>
		</tr>
		<tr>
		<td>| (����� �������) </td>
		<td>- (|) </td>
		<td>x|y</td>
		</tr>
		</table>
	���������� ��������:
&not; &and; &or; &oplus; &rarr; &harr; &darr; |
			</pre>
		</div>
        <div id="content_3" class="content">
		<pre>
����������� ��� � ����� ����������� ���, � ������� ���������� ����������� ���������� ��������� 
����������.
		</pre>
		</div>

    </div>
</div>
</body>
</HTML>