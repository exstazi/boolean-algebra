<?php	
require_once 'library.php';
require_once 'check.php';

if( empty( $_GET['formula'] ) ) 
{
	echo '������!';
	exit();
}
$check = new convert( $_GET['formula'], true );

if( $check->error )
{
	echo $check->error;
	exit();
}

$print = $check->cout;
$P = $check->P;			//������� � ���������� �����
$N = $check->N;			//���-�� ����������
$arr = $check->ARR;		//����� ����������


if( empty( $P ) || empty( $N ) || empty( $arr ) )
{
	echo "������!";
	exit();
}
require_once 'class.php';

$begin = microtime();  
$arrbegin = explode(" ",$begin);  
$allbegin = $arrbegin[1] + $arrbegin[0];

$new = new bool_function( $P,true,false );
$new->N = $N;
$new->ARR = $new->FULL_ARR = $arr;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<meta name="keywords" content="�������������� ������,������,������� ������� �������,<?=$print;?>,������� ���������� ������,������� ����������,������� ���������,���������� ���������,����,����,���,���" />
<meta name="description" content="" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title><?=$print?></title>
<link href="site/default.css" rel="stylesheet" type="text/css" media="all" />
</head>
<body>
<div id="wrapper">
	<div id="page-wrapper">
		<div id="page">
			<div id="content">
				<div>
					<h2>������� ��������</h2>
					<p class='pa'>�������:<b> <?=$print;?></b><br />
					������� ����������: </p>
					<div class='pb'><?=$new->print_table();?></div>
					<p class='pa'>����: <?=prints( $new->SDNF );?><br />
					����: <?=prints( $new->SKNF );?><br />
					����: <br />
					������� ���������: <?=$new->polinom_to_SDNF();?><br />
					�������� ������ �����������: <br /></p>
					
					<div class='pb'><?=$new->print_table_zamkn();?></div>
				</div>
			</div>
		</div>
		<?php
			$stop = microtime();  
			$arrend = explode(" ",$stop);  
			$allend = $arrend[1] + $arrend[0];  
			$alltime = $allend - $allbegin;  
		?>
	</div>
	<div id="footer-wrapper">
		<div id="footer-content">
			<h2>���������</h2>
			<p class='pa'><b>���������� �������� ���������</b><br />1) ����� �������������� ������������� </p>
			<div class='pb'><?=$new->print_polinom();?></div>
			<p class='pa'>2) ����� ��������������� �������������� �� ����</p>
			<div class='pb'><?=$new->print_polinom_to_SDNF();?></div>
			<p class='pa'><b>���������� ����������� ��� ������� �������������� ����</b></p>
			<div class='pb'><?=$new->print_method_kart();?></div>
			<div class='pb'><?=$new->print_method_kart_karno();?></div>
		</div>
	</div>
</div>
<br /><br /><br />
</body>
</html>
