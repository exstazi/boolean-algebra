<?php
/*
��� ���� ����� ������ ��� �������
�������������� ��������������(�������������)
*/

//����� � ��������� ��������� � �����������
//----------------------------------------------

include 'class/basic.php';


//�������� ������� ���������� ��� ���������� �������( ������ � ����������� �������)
//----------------------------------------------

include 'class/create_table.php';


//�������� �������� ���������
//----------------------------------------------

include 'class/polinom.php';

//���������� ������� � �������� �����������
//----------------------------------------------

include 'class/table_zamkn.php';


//���������� ����������� ���
//----------------------------------------------

include 'class/begin_mdnf.php';

//���������� ����������� ��� ������� �������������� ��������������
//----------------------------------------------

include 'class/aig.php';

//����� ������� ������� �� �������
//----------------------------------------------

class bool_function extends aig
{
	function bool_function( $formula,$version,$check = false )
	{
		$this->FORMULA = $formula;
		$this->VERSION = $version;
		if( $check == true )
		{
			$this->edit( );
		}
	}
	
	function print_table( )
	{
		if( !isset( $this->create_table_progress ) )
			$this->who_begin();
		$n = sizeof( $this->TABLE[0] );
		$j = pow( 2,$this->N );
		echo '<table border=1 id=ourtable><tr>';
		for( $i = 0; $i < $n; $i++ )
			echo '<td>'.$this->FULL_ARR[$i].'</td>';
		echo '</tr>';
		for( $i = 0; $i < $j; $i++ )
		{
			echo '<tr>';
			for( $k = 0; $k < $n; $k++ )
			{
				echo '<td>'.$this->TABLE[$i][$k].'</td>';
			}
			echo '</tr>';
		}
		echo '</table>';
	}
	
	function print_polinom()
	{
		echo '������� ��������� ����� ��������� ���: <br />P('.implode( $this->ARR,',' ).') = a<sub>0</sub>';
		$x = $this->generate_nabor( implode( $this->ARR,'' ) );
		$y = '';
		for( $i = 1; $i <= $this->N; $i++ )
			$y.= $i;
		$x1 = $this->generate_nabor( $y );
		$n = sizeof( $x );
		for( $i = 0; $i < $n; $i++ )
		{
			echo ' &oplus; a<sub>'.$x1[$i].'</sub>'.$x[$i];
		}
		echo '<br /><br />';
		
		$x = $this->who_polinom();
		
		echo '<br /><br />���������:<br />P('.implode( $this->ARR,',').') = '.$x;
		
	}
	
	function print_polinom_to_SDNF()
	{
		echo '�������:<br />  &not;X = X &oplus; 1 <br />		X &or; Y = X &oplus; Y<br /><br />'.prints( $this->SDNF ).' = ';
		$j = strlen( $this->SDNF );
		for( $i = 0; $i < $j; $i++ )
		{
			if( $this->SDNF[$i] == '!' )
			{
				echo '('.$this->SDNF[$i+1].' &oplus; 1 )';
				$i++;
			}
			elseif( $this->SDNF[$i] == '+' )
			{
				echo '&or;';
			}
			else
			{
				echo $this->SDNF[$i];
			}
		}
		$k = $this->polinom_to_SDNF();
		echo ' = '.$k;
		echo '<br /><br />���������: '.$k;
	}
	
	function print_table_zamkn()
	{
		$this->begin_table_zamnkn();
		echo '<table border=1 id=ourtable><tr><td>T0</td><td>T1</td><td>L</td><td>M</td><td>S</td></tr><tr>';
		for( $i = 0; $i < 5; $i++ )
		{
			echo '<td>'.$this->TABLE_ZAMKN[$i].'</td>';
		}
		echo '</tr></table>';
	}
	
	function print_method_kart()
	{
		echo '��� 1. ���������� ������� ������������ ���������: ';
		$arr = $this->generate_table();
		echo '<table border=1 id=ourtable>';
		$n = pow( 2, $this->N );
		for( $i = 0; $i < $n; $i++ )
		{
			echo '<tr><td>'.implode( $arr[$i],'</td><td>' ).'</td></tr>';
		}
		echo '</table>';
		
		echo '<br /><br />��� 2. ����������� �� ������ ������� �� �������� ������������ ����<br />';
		
		$arr = $this->minus_sdnf( $arr );
		
		echo '<table border=1 id=ourtable>';
		for( $i = 0; $i < $n; $i++ )
		{
			echo '<tr><td>'.implode( $arr[$i],'</td><td>' ).'</td></tr>';
		}
		echo '</table>';
		
		echo '<br /><br />��� 3. �������� ��������� ����:<br />';
		
		$this->analiz_mdnf( $arr );
		
		echo $this->MDNF;
		
	}
	
	function print_method_kart_karno()
	{
		echo '<br />����� �����<br />';
		/*
		2 - 2x2
		3 - 2x4
		4 - 4x4
		5 - 2x(4x4)		
		*/
		if( $this->N == 2 )
		{
			echo '<table border=1 id=ourtable><tr><td>'.$this->ARR[0].'\ '.$this->ARR[1].'</td><td>0</td><td>1</td></tr><tr><td>0</td><td></td><td></td></tr><tr><td>1</td><td></td><td></td></tr></table>';
		}
		
		
		
	}
	
}


class bool_vector extends begin_mdnf
{
	function bool_vector( $vector,$cheeck = false )
	{
	
	}
}

/*
����� ������� ����� ������� ������������:
	-	print_table();
	-	print_polinom();
	-	print_table_zamkn();
	-	print_method_kart();
*/


?>