<?php
class polinom extends create_table
{
	function who_polinom()
	{
		$arr = $this->create_polinom();
		$j = '';
		for( $i = 1; $i <= $this->N; $i++ )
		{
			$j.= $i;
		}
		$x = $this->generate_nabor( $j );
		$j = pow( 2, $this->N );
		$result = '';
		$f = true;
		if( $arr[0] == 1 )
		{
			$result = '1 &oplus; ';
		}
		
		print_r( $arr );
		
		for( $i = 0; $i < $j; $i++ )
		{
			if( $arr[$x[$i]] == 1 )
			{
				$r = strlen( $x[$i] );
				if( $r > 1 )
					$f = false;
				for( $q = 0; $q < $r; $q++ )
				{
					$result.=  $this->ARR[$x[$i][$q] - 1];
				}
				$result.= ' &oplus; ';
			}
		}
		if( $f )		$this->TABLE_ZAMKN[2] = '+';
		return substr( $result,0,strlen( $result ) - 8 );
	}
	
	private function create_polinom()
	{
		$arr = $this->create_boll_nabor( $this->N );
		$arr_polinom = Array( );
		$n = pow( 2,$this->N );		
		$arr_polinom['0'] = $this->VECTOR[0];
		echo 'P('.implode( $arr[0],',' ).') = a<sub>0</sub> = '.$this->VECTOR[0].'<br />';
		
		for( $i = 1; $i < $n; $i++ )
		{
			$k = '';
			for( $j = 1; $j <= $this->N; $j++ )
			{
				if( $this->TABLE[$i][$j-1] == 1 )	$k.= $j;
			}
			echo $k + " ";
			if( strlen( $k ) == 1 )
			{
				$arr_polinom[$k] = ( $this->VECTOR[$i] + $arr_polinom[0] ) % 2;
				echo 'P('.implode( $arr[$i],',' ).') = a<sub>0</sub> &oplus; a<sub>'.$k.'</sub> = '.$this->VECTOR[$i].' = > a<sub>'.$k.'</sub> = '.$arr_polinom[$k].'<br />';
			}
			else
			{
				$nabor = $this->generate_nabor( $k );
				$x = sizeof( $nabor );
				$q = $arr_polinom[0];
				echo 'P('.implode( $arr[$i],',' ).') = a<sub>0</sub> ';
				for( $j = 0; $j < $x - 1; $j++ )
				{
					echo '&oplus; a<sub>'.$nabor[$j].'</sub>';
					$q += $arr_polinom[$nabor[$j]];
				}
				$arr_polinom[$k] = ( $q + $this->VECTOR[$i] )% 2;
				echo ' &oplus; a<sub>'.$k.'</sub> = '.$this->VECTOR[$i].' = > a<sub>'.$k.'</sub> = '.$arr_polinom[$k].'<br />';
			}
			
		}
		return  $arr_polinom;
	}
	
	public function polinom_to_SDNF()
	{
		$arr1 = explode( '+',$this->SDNF );
		if( sizeof( $arr1 ) == 1 ) return $arr1[0];
		$arr2 = Array();
		$z = array_pop( $arr1 );
		while( !empty( $z ) )
		{
			$n = strlen( $z );
			$f = false;
			$o = 0;
			for( $i = 0; $i < $n; $i++ )
			{
				if( $z[$i] == '!' )
				{
					if( $n == 2 )
					{
						array_push( $arr2,$z[1] );
						array_push( $arr2,1 );
					}
					else
					{
						array_push( $arr1, substr( $z,0,$i ).substr( $z,$i+1 ) );
						array_push( $arr1, substr( $z,0,$i ).substr( $z,$i+2 ) );						
					}
					break;
				}
				else
				{
					$o++;
				}
			}
			if( $f || $o == $n )
			{
				array_push( $arr2,$z );
			}
			$z = array_pop( $arr1 );
		}
		//print_a( $arr2 );
		return $this->sortirovaka( $arr2 );
	}
	
	function sortirovaka( $arr2 )
	{
		if( sizeof( $arr2 ) == 1 )			return array_pop( $arr2 );
		
		$arr = Array();
		$n = sizeof( $arr2 );
		for( $i = 0; $i < $n-1; $i++ )
		{
			if( $arr2[$i] != '-' )
			{
				$k = 1;
				for( $j = $i+1; $j < $n; $j++ )
				{
					if( $arr2[$i] == $arr2[$j] )
					{
						$k++;
						$arr2[$j] = '-';
					}
				}
				if( $k % 2 == 1 )
				{
					array_push( $arr, $arr2[$i] );
				}
			}
		}
		sort( $arr );
		return implode( $arr,' &oplus; ' );
	}
	
	
}
?>