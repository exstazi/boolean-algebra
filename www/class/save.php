<?php
/*
В какой файл будет записано | Сколько в этот файл уже записано | Максимум сколько можно записать
*/
class db
{
	var $ARR = Array();		//Настройки базы данных
	
	function db( )
	{
		$this->connect();
	}
	
	private function connect()
	{
		$file = fopen( 'save/index.txt', 'r' );
		$mytext = fgets( $file, 999 );
		$this->ARR = explode( '|', $mytext );
		fclose( $file );
	}
	
	private function edit( )
	{
		$file = fopen( 'save/index.txt', 'w+' );
		if( $this->ARR[1] > $this->ARR[2] )
		{
			$arr = explode( '.',$this->ARR[0] );
			$this->ARR[0] = ( $arr[0] + 1 ).'.txt';
			$this->ARR[1] = 0;
		}
		fwrite( $file, $this->ARR[0].'|'.$this->ARR[1].'|'.$this->ARR[2] );
		fclose( $file );
	}
	
	public function insert( $text )
	{
		$file = fopen( 'save/'.$this->ARR[0], 'a' );
		fwrite( $file, $text.PHP_EOL );
		$this->ARR[1]++;
		fclose( $file );
		$this->edit();
	}
	
	public function GetRealIp()
	{
		if (!empty($_SERVER['HTTP_CLIENT_IP'])) 
		{
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		}
		elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
		{
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		}
		else
		{
			$ip = $_SERVER['REMOTE_ADDR'];
		}
		return $ip;
	}
}
?>
