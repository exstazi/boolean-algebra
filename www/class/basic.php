<?php
class basic
{
	var $VERSION = false;			//������ ���������
	var $FORMULA = '';				//������� ��� ���������(������������� ������)
	var $VECTOR = Array();			//������	
	var $TABLE = Array();			//������� � ��������� �������
	var $N = 0; 					//���-�� ����������
	var $ARR = Array();				//����� ����������
	var $SDNF = '';
	var $SKNF = '';
	var $MDNF = '';
	var $POLINOM = '';				//������� ���������
	var $TABLE_ZAMKN = Array( '-','-','-','-','-' );
	
	//���������� ��������� ����� 
	//----------------------------------------------
	
	function create_boll_nabor( $n )
	{
		$arr = Array( );
		$result = Array( );
		$j = pow( 2,$n );
		for( $i = 0; $i < $n; $i++ )
		{
			$arr[$i] = 0;
		}
		$f = false;
		for( $i = 0; $i < $j; $i++ )
		{	
			$result[] = $arr;
			$arr[sizeof( $arr ) - 1] += 1;
			for( $k = sizeof( $arr ) - 1; $k >= 0; $k-- )
			{		
				if( $f == false and $arr[$k] != 2 )	break;
				if( $f )           {	$arr[$k]++;		$f = false;		}
				if( $arr[$k] == 2 ){	$arr[$k] = 0;	$f = true;		}
			}
		}
		return $result;
	}
	
	//�������� ���������� �� ��������
	//----------------------------------------------
	
	function zamena( $x,$arr_p,$arr_z )
	{
		$p = strlen( $x );
		$k = sizeof( $arr_p );													//���������� ����������
		for( $i = 0;$i < $p; $i++ )
		{
			for( $j = 0; $j < $k; $j++ )
			{
				if( $x[$i] == $arr_p[$j] )	{ $x[$i] = $arr_z[$j]; break; }		//���� ���������� � ��������
			}
		}
		return $x;
	}
	
	//������������ ���� ����� �������������� ��������� �( ������� ��������� ������ �������� ������ �� ���� � ������ �������� )
	//----------------------------------------------
	
	function arifmetic( $x )
	{
		$arr = Array();
		$j = strlen( $x );
		for( $i = 0; $i < $j; $i++ )
		{
			if( is_numeric( $x[$i] ) )	array_push( $arr,$x[$i] );
			else
			{
				$str = 1;
				if( $x[$i] == '!' )
				{
					$str = ( array_pop( $arr ) + 1 ) % 2;
				}
				elseif( $x[$i] == '+' )
				{
					$str = array_pop( $arr ) + array_pop( $arr );
					if( $str > 1 ) 	$str = 1;
				}
				elseif( $x[$i] == '*' )
				{
					$str = array_pop( $arr )*array_pop( $arr );
				}
				elseif( $x[$i] == '/' )
				{
					$str = ( array_pop( $arr )+array_pop( $arr ) )%2;
				}
				elseif( $x[$i] == '?' )
				{
					$z = array_pop( $arr );
					if( ( array_pop( $arr ) - $z )  == 1 )	$str = 0;
				}
				elseif( $x[$i] == '%' )
				{
					if( ( array_pop( $arr ) + array_pop( $arr ) ) == 1 ) $str = 0;
				}
				elseif( $x[$i] == '^' )
				{
					if( ( array_pop( $arr ) + array_pop( $arr ) ) != 0 ) $str = 0;
				}
				elseif( $x[$i] == '|' )
				{
					if( ( array_pop( $arr ) + array_pop( $arr ) ) == 2 ) $str = 0;
				}
				array_push( $arr,$str );
			}
		}
		return array_pop( $arr );
	}
	
	//���������� ������ ������������ ���������� �������� ������ � � ���������� �� � �������� �������
	//----------------------------------------------
	
	function generate_nabor( $x )
	{
		$N = strlen( $x );
		$b = Array();
		
		for( $i = 0; $i < $N; $i++ )
			$b[] = $x[$i];
		$n = pow( 2,$N ) - 1;
		for( $i = 0; $i < $n; $i++ )
		{
			for( $k = 0; $k < $N; $k++ )
			{
				if( $b[$i][strlen( $b[$i] ) - 1] == $b[$k] )
					break;
			}
			$j = $k+1;
			for( ; $j < $N; $j++ )
			{
				$b[] = $b[$i].$b[$j];
			}
		}
		return $b;
	}
	
	//���������� ������ ������������ ���������� �������� ������� � � ���������� �� � �������� �������
	//----------------------------------------------
	
	function generate_nabor_arr( $b )
	{
		$N = sizeof( $b );	
		
		$n = pow( 2,$N ) - 1;
		for( $i = 0; $i < $n; $i++ )
		{
			for( $k = 0; $k < $N; $k++ )
			{
				if( $b[$i] == $b[$k] )
					break;
			}
			$j = $k+1;
			for( ; $j < $N; $j++ )
			{
				$b[] = $b[$i].$b[$j];
			}
		}
		
		return $b;
	}
}
?>