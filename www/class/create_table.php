<?php
class create_table extends basic
{

	#������ ��� ��������� � ���( � ������� ������� ��� ��� ��� ����� �������� ��������������� �������� )
	function who_begin( )
	{
		$this->create_table_progress = true;
		$this->TABLE = $this->create_boll_nabor( $this->N );			//������ ������ � ��������� �������
		if( $this->VERSION == true )
		{
			$this->create_full();
		}
		else
		{
			$this->create_not_full();
		}
		//���������� ���� ����� �������� ��� ����� �������
		$k = sizeof( $this->TABLE[0] );
		$p = pow( 2,$this->N );
		for( $i = 0; $i < $p; $i++ )
		{
			$this->VECTOR[$i] = $this->TABLE[$i][$k-1];
		}
		$this->S_NF();
	}
	
	//FULL - user
	//----------------------------------------------------
	
	private function create_full()
	{
		$x = $this->FORMULA;
		$n = strlen( $x );
		$p = pow( 2,$this->N );
		$arr = Array();
		$arr2 = $this->ARR;				//������ ���� ����������
		$H = $this->N + 1;				//����� ���������� � �������
		
		for( $i = 0; $i < $n; $i++ )
		{
			if( letter( $x[$i] ) || is_numeric( $x[$i] ) )
			{
				array_push( $arr,$x[$i] );
			}
			else
			{
				$a = array_pop( $arr );
				if( $x[$i] == '!' )
				{
					if( is_numeric( $a ) && $a > 1 )		$this->FULL_ARR[] = '&not'.$this->FULL_ARR[$a-1];
					else									$this->FULL_ARR[] = '&not;'.$a;					
					$w = $a.'!';
				}
				else
				{
					$b = array_pop( $arr );
					if( is_numeric( $b ) && $b > 1 && is_numeric( $a ) && $a > 1 )
						$this->FULL_ARR[] = '('.prints( $this->FULL_ARR[$b-1].$x[$i].$this->FULL_ARR[$a-1] ).')';
					elseif( is_numeric( $b ) && $b > 1 )
						$this->FULL_ARR[] = '('.prints( $this->FULL_ARR[$b-1].$x[$i].$a ).')';
					elseif( is_numeric( $a ) && $a > 1 )
						$this->FULL_ARR[] = '('.prints( $b.$x[$i].$this->FULL_ARR[$a-1] ).')';
					else
						$this->FULL_ARR[] = '('.prints( $b.$x[$i].$a ).')';
					$w = $b.$a.$x[$i];
				}
				for( $q = 0; $q < $p; $q++ )
				{
					$this->TABLE[$q][] = $this->arifmetic( $this->zamena( $w,$arr2,$this->TABLE[$q] ) );
				}				
				$arr2[] = $H;
				array_push( $arr,$H );
				$H++;
			}
		}
	}
	
	//NOT FULL - user
	//----------------------------------------------------
	
	private function create_not_full()
	{
		$j = pow( 2, $this->N );
		for( $i = 0; $i < $j; $i++ )
		{
			$this->TABLE[$i][] = $this->arifmetic( $this->zamena( $this->FORMULA,$this->ARR,$this->TABLE[$i] ) );
		}
		$this->FULL_ARR[] = 'f';
	}
	
	//��������������� �������
	//----------------------------------------------------
	private function S_NF()
	{
		$n = sizeof( $this->ARR ) - 1;
		$j = pow( 2,$this->N );
		$a = $b = Array();
		
		for( $i = 0; $i < $j; $i++ )
		{
			$x = '';
			if( $this->VECTOR[$i] == 1 )
			{
				//SDNF
				for( $o = 0; $o < $this->N; $o++ )
				{
					if( $this->TABLE[$i][$o] == 0 )
						$x.= '!';
					$x.= $this->ARR[$o];
				}
				$a[] = $x;
			}
			else
			{
				//SKNF
				for( $o = 0; $o < $this->N; $o++ )
				{
					if( $this->TABLE[$i][$o] == 1 )
						$x.= '!';
					$x.= $this->ARR[$o].'+';
				}
				$x[strlen($x)-1] ='';
				$b[] = '('.$x.')';
			}
		}
		$this->SDNF = implode( $a,'+' );
		$this->SKNF = implode( $b,'*' );
	}
	
	
}
?>