<?php
class table_zamnkn extends polinom
{
	function begin_table_zamnkn()
	{
		if( $this->VECTOR[0] == 0 )		$this->TABLE_ZAMKN[0] = '+';
		if( $this->VECTOR[sizeof( $this->VECTOR ) - 1] == 1 )		$this->TABLE_ZAMKN[1] = '+';
		if( $this->M() )				$this->TABLE_ZAMKN[3] = '+';
		if( $this->S() )				$this->TABLE_ZAMKN[4] = '+';
	}
	
	private function M()
	{
		$n = pow( 2,$this->N );
		if( $this->VECTOR[$n-1] == 0 )
		{
			for( $i = 0; $i < $n-1; $i++ )
			{
				if( $this->VECTOR[$i] == 1 )
					return false;
			}
			return true;
		}
		
		for( $i = $n - 1; $i > 0; $i-- )
		{
			for( $j = $i - 1; $j >= 0; $j-- )
			{
				for( $k = $this->N-1; $k >= 0; $k-- )
				{
					if( $this->TABLE[$i][$k] < $this->TABLE[$j][$k] )
						break;
				}
				if( $k == -1 )
				{
					if( $this->VECTOR[$i] < $this->VECTOR[$j] )
						return false;
				}
			}
		}
		return true;
	}
	
	
	private function S()
	{
		$n = pow( 2,$this->N );
		for( $i = 0; $i < $n/2; $i++ )
		{
			if( $this->VECTOR[$i] == $this->VECTOR[$n-$i-1] )
				return false;
		}
		return true;
	}
}
?>